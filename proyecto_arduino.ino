#define SensorLED   9      // Se declara el pin del led incorporado en el arduino
#define SensorINPUT  3      // Se declara el pin al que se conecta el sensor   
unsigned char state = 0;    // Se declara un estado de vibración  


const int buttonPin = 13;     // Pin al que se conectará el botón
int buttonState = 0;   

int buzzer = 9; 

void setup() {
  pinMode(SensorLED, OUTPUT);
  pinMode(SensorINPUT, INPUT);
  pinMode(buzzer,OUTPUT); 
  pinMode(buttonPin, INPUT);

  attachInterrupt(1, blink, FALLING); // dispara el estado para que el led se encienda
}

void loop() {
  unsigned char i;
  buttonState = digitalRead(buttonPin);
  
  if (state != 0) {                 // si el estado del sensor es distinto a 0
    state = 0;
    digitalWrite(SensorLED, HIGH);  // el led se enciende
    delay(500);

    //output an frequency
    while(buttonState == LOW) {
      digitalWrite(buzzer,HIGH);
      delay(1);//wait for 1ms
      digitalWrite(buzzer,LOW);
      delay(1);//wait for 1ms
      buttonState = digitalRead(buttonPin);
    }
    // si se presiona el botón, se detiene la emisión de sonido y se apaga el led
    noTone(buzzer);
    digitalWrite(SensorLED, LOW);
  } 
  else
    digitalWrite(SensorLED, LOW);   // el led se apaga
    state = 0;
}

void blink() {
    state++;
}
